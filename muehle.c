/*

        1     2     3     4     5     6
  A     X     X     X
  B     X     X     X
  C     X     X     X
  D     X     X     X     X     X     X
  E     X     X     X
  F     X     X     X
  G     X     X     X


        1     2     3     4     5     6     7

  A     O-----------------O-----------------O
        |                 |                 |
  B     |     O-----------O-----------O     |
        |     |           |           |     |
  C     |     |     O-----O-----O     |     |
        |     |     |           |     |     |
  D     O-----O-----O           O-----O-----O
        |     |     |           |     |     |
  E     |     |     O-----O-----O     |     |
        |     |                       |     |
  F     |     O-----------O-----------O     |
        |                 |                 |
  G     O-----------------O-----------------O


*/

#include <stdio.h>
#include <stdlib.h>

  struct Spielstein {
      int status_verfuegbarkeit;
      int x;
      int y;
  };

  struct Spieler {
      char name[21];
      struct Spielstein stein[9];
  };


void eingabe_position(struct Spieler *spieler);


int main(int argc, char const *argv[]) {

int a = 5;

struct Spieler *spieler_eins, *spieler_zwei, *aktueller_sp;

spieler_eins = malloc(sizeof(struct Spieler));
spieler_zwei = malloc(sizeof(struct Spieler));
aktueller_sp = malloc(sizeof(struct Spieler));

// ----- Initialisiere Spielernamen -----

printf("Spieler 1, wie moechtest du heissen ? : ");
scanf("%20s", spieler_eins->name);

printf("Spieler 2, wie moechtest du heissen ? : ");
scanf("%20s", spieler_zwei->name);

// --------------------------------------

*aktueller_sp = *spieler_eins;

eingabe_position(aktueller_sp);

printf("%i\n", spieler_eins->stein->x );
printf("%i\n", spieler_eins->stein->y );

printf("%i\n", aktueller_sp->stein->x );
printf("%i\n", aktueller_sp->stein->y );

printf("\n");



//a = check_position(eingabe[1][1], hit_matrix[3][8]);

printf("PRINT: a : %i\n", a);

// ----- Speicher freigeben -----

  free(spieler_eins);
  free(spieler_zwei);

// ------------------------------

  return 0;
}


  void eingabe_position(struct Spieler *spieler) {

    printf("%s gib die Position an (x - Position): ", spieler->name );
    scanf(" %i", &spieler->stein->x);

    printf("%s gib die Position an (y - Position): ", spieler->name );
    scanf(" %i", &spieler->stein->y);

    printf("%i\n", spieler->stein->x );
    printf("%i\n", spieler->stein->y );

  }
